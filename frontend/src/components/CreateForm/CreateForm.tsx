import React from "react";
import { Formik, Form, Field, FormikHelpers, FormikErrors } from "formik";
import Button from "../Button";
import * as Yup from "yup";
import { createNewspost } from "../../store/action/newsposts/newspostsAction";
import { useTypedDispatch } from "../../store/Store";
import { useNavigate } from "react-router-dom";
interface CreateValues {
  title: string;
  text: string;
}
const CreateForm: React.FC = () => {
  const dispatch = useTypedDispatch()
  const navigate = useNavigate();
  const initialValues: CreateValues = { title: "", text: "" };
  const handleSubmit = async (values: CreateValues, actions:FormikHelpers<CreateValues>) => {    
    try {
      const PostSchema = Yup.object().shape({
        title: Yup.string()
          .min(2, "Too Short!")
          .max(50, "Too Long!")
          .required("Title is required"),
      
        text: Yup.string()
          .min(2, "Too Short!")
          .max(1000, "Too Long!")
          .required("Post is required"),
      });
      await PostSchema.validate(values, { abortEarly: false });
      const id = await dispatch(createNewspost('/api/newsposts', values))
      actions.setSubmitting(false);
      navigate(`/newsposts/${id}`);
    } catch (errors) {
      if (Yup.ValidationError.isError(errors)) {
        const formErrors: FormikErrors<any> = {};
        errors.inner.forEach((error) => {
          if (typeof error.path === 'string') {
            formErrors[error.path] = error.message;
          }
        });
        actions.setErrors(formErrors);
      }
    }
  };
  return (
    <div className="max-w-[500px] mx-auto my-[20px]">
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
    >
      <Form className="flex flex-col">
        <div className="flex flex-col">
          <label htmlFor="title">Title</label>
          <Field id="title" name="title" placeholder="Title" />
        </div>
        <div className="flex flex-col">
          <label htmlFor="text">Post</label>
          <Field id="text" name="text" placeholder="text" />
        </div>
        <Button id='createPost'/>
      </Form>
    </Formik>
    </div>
  );
};

export default CreateForm;
