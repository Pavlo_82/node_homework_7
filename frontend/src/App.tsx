import React, { useEffect } from "react";
import {Route, Routes} from 'react-router-dom'
import Home from './pages/Home/'
import NewspostDetailPage from './pages/NewspostDetailPage'
import CreateNewsPostPage from './pages/CreateNewsPostPage'
import { useTypedDispatch } from "./store/Store";
import { getNewsPosts } from './store/action/newsposts/newspostsAction';
import EditPostPage from "./pages/EditPostPage";

const App = () => {
  const dispatch = useTypedDispatch();
  useEffect(() => {
    const init = async () => {
      dispatch(getNewsPosts("/api/newsposts?page=1&size=6"));
    };
    init();
  }, [dispatch]);
  return (
    <Routes>
      <Route path="/" element={<Home />}/>
      <Route path="/newsposts/create" element={<CreateNewsPostPage />}/>
      <Route path="/newsposts/:id" element={<NewspostDetailPage />}/>
      <Route path="/newsposts/:id/edit" element={<EditPostPage />}/>
      <Route element={<h1>Not Found</h1>}/>
    </Routes>
  )
}
export default App