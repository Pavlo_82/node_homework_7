import express from 'express';

class App {
  public app: express.Application;
  public port: number;

  constructor(controllers:Array<any>, port: number) {
    this.app = express();
    this.port = port;
    this.app.use(express.static('frontend/build'));
    this.initializeMiddlewares();
    this.setupRoutes(controllers);
  }

  private initializeMiddlewares() {
    this.app.use(express.static('public'));
  }

  private setupRoutes(controllers:Array<any>) {
    this.app.get('/', (req, res) => {
      res.send('Hello, World!');
    });

    controllers.forEach(controller=>{
      this.app.use(`/api${controller.path}`, controller.router);
    })    
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
    });
  }
}

export default App;
