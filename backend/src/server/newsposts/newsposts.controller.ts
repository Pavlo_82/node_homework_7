import NewspostsService from '../../bll/newsposts/newsposts.service.ts';
import express from 'express';
import { INews } from 'types/params.interface.ts';
import { Service } from 'typedi';

@Service()
class NewspostsController {
  public path = '/newsposts';
  public router = express.Router();

  constructor(private newspostsService: NewspostsService) {
    this.initializeRoutes();
  }

  public initializeRoutes() {
    this.router.get('/', this.getAllNewspost);
    this.router.get('/:id', this.getNewspostById);
    this.router.post('/', this.createANewspost);
    this.router.put('/:id', this.updateANewspost);
    this.router.delete('/:id', this.deleteANewspost);
  }

  getNewspostById = async (
    request: express.Request,
    response: express.Response,
  ) => {
    const id: string = request.params.id;
    const newspost = await this.newspostsService.getById(id);
    response.send(newspost);
  };

  getAllNewspost = async (
    request: express.Request,
    response: express.Response,
  ) => {
    const params = {
      size: request.query.size ? Number(request.query.size) : null,
      page: request.query.page ? Number(request.query.page) : null,
      filter: request.query.filter || {},
    };
    const pagedNewsposts = await this.newspostsService.getAllNewspost(params);
    response.send(pagedNewsposts);
  };

  createANewspost = async (
    request: express.Request,
    response: express.Response,
  ) => {
    const newspost: INews = request.body;
    try {
      const createdNewspost = await this.newspostsService.createANewspost(
        newspost,
      );
      response.send(createdNewspost);
    } catch (e: any) {
      throw new Error(e);
    }
  };

  updateANewspost = async (
    request: express.Request,
    response: express.Response,
  ) => {
    const newsPost: INews = request.body;
    const id: string = request.params.id;

    try {
      const updatedNewspost = await this.newspostsService.updateANewspost(
        id,
        newsPost,
      );
      response.send(updatedNewspost);
    } catch (e: any) {
      throw new Error(e);
    }
  };

  deleteANewspost = async (
    request: express.Request,
    response: express.Response,
  ) => {
    const id: string = request.params.id;
    try {
      const message = await this.newspostsService.deleteANewspost(id);
      response.send(message);
    } catch (e: any) {
      throw new Error(e);
    }
  };
}

export default NewspostsController;
