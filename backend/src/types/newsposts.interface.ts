export interface Newspost {
  title: string;
  text: string;
}

export interface PagedNewsPosts {
  newsposts: Newspost[];
  total: number;
  size: number;
  page: number;
}
