import convict from 'convict';

interface MyConfig {
  PORT: number;
  HOST: string;
}
const config = convict<MyConfig>({
  PORT: {
    doc: 'The port to bind.',
    format: Number,
    default: 8000,
    env: 'PORT',
  },
  HOST: {
    doc: 'The host to bind.',
    format: String,
    default: 'http://localhost',
    env: 'HOST',
  },
});

// Валидация конфигурации
config.validate({ allowed: 'strict' });

export default config;