import { PagedNewsPosts } from '../../types/newsposts.interface.ts';
import { IError, INews, ITable, Params } from '../../types/params.interface.ts';
import { Service } from 'typedi';
import { FileDB } from '../../fileDb/fileDb.ts';

@Service()
class NewspostsRepository {
  private table: ITable;
  constructor() {
    const fileDBInstance = FileDB.getInstance();
    this.table = fileDBInstance.getTable('db');
  }
  getAllNewspost = async (params: Params): Promise<PagedNewsPosts> => {
    const array = await this.table.getAll();
    let result = [...array];

    if (params.filter.author) {
      result = array.filter((item: INews) =>
        item.title.includes(params.filter.title),
      );
    }

    if (params.size != null && params.page != null) {
      result = result.splice(params.page * params.size, params.size);
    }

    return {
      total: array.length,
      newsposts: result,
      size: params.size || 5,
      page: params.page || 1,
    };
  };

  getById = async (id: string): Promise<IError | INews> => {
    const newPost = await this.table.getById(id);

    return await newPost;
  };

  createANewspost = async (newspost: INews): Promise<INews> => {
    const newPost = await this.table.create(newspost);
    return await newPost;
  };

  updateANewspost = async (id: string, newspost: INews): Promise<IError | INews> => {
    const newPost = await this.table.update(id, newspost);
    return await newPost;
  };

  deleteANewspost = async (id: string): Promise<IError | INews> => {
    const newPost = await this.table.delete(id);
    return await newPost;
  };
}

export default NewspostsRepository;
